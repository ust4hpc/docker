FROM docker.io/remche/bash-notebook:0.9 as builder

USER root

RUN apt-get update && apt-get -y install gcc

USER jovyan
# Install Openstack CLI tools
RUN pip install --install-option="--prefix=/tmp/install" \
      #python-cinderclient \
      #python-glanceclient \
      #python-keystoneclient \
      #python-novaclient \
      #python-neutronclient \
      python-openstackclient

FROM docker.io/remche/bash-notebook:0.9

ARG BIN_PATH=/usr/local/bin
ARG KUBECTL_VERSION=v1.19.4
ARG HELM_VERSION=v3.4.1
ARG TERRAFORM_VERSION=0.13.5

# Set openstack output max width
ENV CLIFF_MAX_TERM_WIDTH=115

# Copy Openstack CLI tools
COPY --from=builder /tmp/install $CONDA_DIR
RUN fix-permissions $CONDA_DIR

USER root

RUN apt-get update && apt-get -y install openssh-client \
 && rm -rf /var/lib/apt/lists/*

COPY entrypoint_override.sh /entrypoint_override.sh

# Install K8S and tools
RUN wget https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl -O $BIN_PATH/kubectl && \
    chmod +x $BIN_PATH/kubectl

RUN wget https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens -O $BIN_PATH/kubens && \
    chmod +x $BIN_PATH/kubens

RUN wget https://raw.githubusercontent.com/johanhaleby/kubetail/master/kubetail -O $BIN_PATH/kubetail && \
    chmod +x $BIN_PATH/kubetail

RUN mkdir -p /etc/bash_completion.d && kubectl completion bash > /etc/bash_completion.d/kubectl

# Install Helm
RUN wget https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz -O - | tar xz -C $BIN_PATH --strip=1 linux-amd64/helm

# Install Terraform
RUN wget https://releases.hashicorp.com/terraform/$TERRAFORM_VERSION/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O - | zcat > $BIN_PATH/terraform && \
    chmod +x $BIN_PATH/terraform


USER jovyan
